use strict;
use Data::Dumper;
use DDP;

my @table;
my $i = 0;

while (<>) {
	chop;
	push(@{$table[$i]}, split(/;/, $_)); 
	$i += 1;
}

print Dumper(@table);
p(@table);
