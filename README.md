# One-liners in perl (h/w 1)
Домашнее задание по Perl.

## Get it
Просто склонировать репу:
```
git clone https://krautcat@bitbucket.org/technopark_odisharia/one-liners.git
```

## Usage

### Задание 1
Выполнить скрипт как исполняемый файл:
```
./ls-l-to-file
```

### Задание 2
Выполнить скрипт как исполняемый файл:
```
./filter-files
```

### Задание 3
Запускаем программу `structure-dump.pl` при помощи интерпретатора Perl:
```
perl structure-dump.pl file
```

### Задание 4
Необходимо запустить скрипт из задания 3 с дебаггером:
```
perl -d structure-dump.pl file
```
Затем выполнить команды из файла `debug-scenario`, используя дебаггер:
```
source debig-scenario
```

## Dependencies
* perl
